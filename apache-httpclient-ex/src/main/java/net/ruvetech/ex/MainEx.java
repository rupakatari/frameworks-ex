package net.ruvetech.ex;

import java.io.IOException;
import java.net.URISyntaxException;

public class MainEx {

    public static void main(String[] args) throws IOException, URISyntaxException {
        ApacheHttpClient apacheHttpClient = new
                ApacheHttpClient();
        apacheHttpClient.getRequest();

        apacheHttpClient.postRequest();
    }
}
