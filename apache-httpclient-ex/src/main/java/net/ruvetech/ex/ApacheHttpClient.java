package net.ruvetech.ex;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class ApacheHttpClient {


    private static final String BASE_URL = "https://reqres.in";

    public void getRequest() throws IOException, URISyntaxException {

        String url = BASE_URL + "/api/users";

        //Given
        URI uri = new URIBuilder(url)
                .setParameter("page", "2")
                .build();
        HttpGet get = new HttpGet(uri);
        get.addHeader("Accept", "application/json");
        System.out.println("Resource locator:URI:" + get.getURI());
        HttpClient httpClient = HttpClientBuilder.create().build();

        //When
        HttpResponse httpResponse = httpClient.execute(get);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        System.out.println("Response code:" + responseCode);
        String body = EntityUtils.toString(httpResponse.getEntity());
        System.out.println("Body: " + body);

        //Then
        assert (responseCode == 200);

    }

    public void postRequest() throws IOException {

        //Given
        String url = BASE_URL + "/api/users";
        StringEntity stringEntity = new StringEntity(convertJson("Venu", "super"));

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);
        System.out.println(post.getURI());
        post.addHeader("Content-Type", "application/json");

        post.setEntity(stringEntity);

        //When
        HttpResponse response = httpClient.execute(post);
        int responseCode = response.getStatusLine().getStatusCode();
        System.out.println("Response code: " + responseCode);
        String body = EntityUtils.toString(response.getEntity());
        System.out.println("Response body:" + body);

    }

    public String convertJson(String name, String job) {
        String jsonData = "{" + "\"name\"" + ":" + "\"" + name + "\"" + "," + "\"" + "job" + "\"" + ":" + "\"" + job + "\"" + "}";
        return jsonData;

    }
}
