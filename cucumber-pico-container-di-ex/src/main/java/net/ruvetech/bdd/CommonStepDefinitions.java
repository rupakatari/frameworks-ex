package net.ruvetech.bdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.ruvetech.data.Store;

public class CommonStepDefinitions {

    private Store store = null;

    public CommonStepDefinitions(Store store) {
        this.store = store;
    }

    @Given("today is Sunday")
    public void todayIsSunday() {
        System.out.println("method: " + " todayIsSunday, " + toString());
        store.setSampleValue("rupa");
    }

    @When("I ask whether it's Friday yet")
    public void iAskWhetherItSFridayYet() {
        System.out.println("method: " + " iAskWhetherItSFridayYet, " + toString());
    }
}
