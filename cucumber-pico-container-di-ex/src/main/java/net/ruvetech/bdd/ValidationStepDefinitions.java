package net.ruvetech.bdd;

import io.cucumber.java.en.Then;
import net.ruvetech.data.Store;
import org.junit.Assert;

public class ValidationStepDefinitions {

    private Store store = null;

    public ValidationStepDefinitions(Store store) {
        this.store = store;
    }

    @Then("I should be told {string}")
    public void iShouldBeTold(String arg) {
        System.out.println(store.getSampleValue());
        Assert.assertEquals("Nope", arg);
    }
}
