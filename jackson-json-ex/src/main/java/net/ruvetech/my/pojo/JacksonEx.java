package net.ruvetech.my.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class JacksonEx {

    public static void main(String[] args) throws IOException {
        toJson();
    }

    public static void fromJson() throws IOException {

        String path = System.getProperty("user.dir") + "/jackson-json-ex/src/main/resources/";
        File file = new File(path + "person.json");
        String jsonString = FileUtils.readFileToString(file);

        ObjectMapper objectMapper = new ObjectMapper();

        Person person = objectMapper.readValue(jsonString, Person.class);// Convert JSON String to  object
        System.out.println(person);
    }

    public static void toJson() throws IOException {

        InternalHandle internalHandle = new InternalHandle();
        internalHandle.setUnique_identifier("af75372ae0cde68b0aeb06f0d8ee16dfe38b4fd02b20cb093c1b44eaf05d02ed");
        internalHandle.setVisibility_marker("DEFAULT");
        internalHandle.setInterface_identifier("CID_PEOPLE_1804145318");

        IdentityHandles[] identityHandles = new IdentityHandles[1];

        IdentityHandles identityHandles1 = new IdentityHandles();
        identityHandles1.setInterface_identifier("CID_PEOPLE_1804145318");
        identityHandles1.setUnique_identifier("af75372ae0cde68b0aeb06f0d8ee16dfe38b4fd02b20cb093c1b44eaf05d02ed");
        identityHandles1.setVisibility_marker("DEFAULT");

        identityHandles[0] = identityHandles1;

        Person person = new Person();
        person.setInternalHandle(internalHandle);
        person.setPersonSpace("IPT");
        person.setCreated("2018-11-04T14:53:15.591346");
        person.setCreatedBy(null);
        person.setIdentityHandles(identityHandles);


        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String json = objectMapper.writeValueAsString(person);
            System.out.println(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
