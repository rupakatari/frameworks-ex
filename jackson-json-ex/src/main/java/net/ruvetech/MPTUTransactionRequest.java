package net.ruvetech;

public class MPTUTransactionRequest {
	private String amount;

	private String aliasName;

	private String authToken;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	@Override
	public String toString() {
		return "ClassPojo [amount = " + amount + ", aliasName = " + aliasName + ", authToken = " + authToken + "]";
	}
}
