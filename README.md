### Introduction
This project is aimed to create small poc to learn different frameworks or library usage.
It has multiple modules and each module as appropriately named helps learning about that framework 
or library usage.

##### commons-configuration2-ex
This module helps learning the apache commons configuration library particularly about the properties file

##### jackson-json-ex
This helps learning java jackson libraries which shall marshall/unmarshall json

##### excel-ex
This helps learning apache poi library which read or write data to excel files or csv

##### apache-httpclient-ex
This helps understanding the http client that could make rest requests

##### cucumber-spring-di-ex
Module to understand spring dependency injection along with cucumber

##### cucumber-rest-assured-ex
Module to understand cucumber with rest client as rest assured
Use the following command to run features using Maven Exec plugin
``
mvn exec:java                                  \
    -Dexec.classpathScope=test                 \
    -Dexec.mainClass=io.cucumber.core.cli.Main \
    -Dexec.args="src/main/resources/features --glue net.ruvetech.bdd"
``

##### Frequent Commands
find . -name '*.idea' -type f -delete
find . -name '*.iml' -type f -delete