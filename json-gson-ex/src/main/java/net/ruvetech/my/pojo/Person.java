package net.ruvetech.my.pojo;

public class Person {

    private InternalHandle internalHandle;

    private String personSpace;

    private String created;

    private String createdBy;

    private IdentityHandles[] identityHandles;

    public IdentityHandles[] getIdentityHandles() {
        return identityHandles;
    }

    public void setIdentityHandles(IdentityHandles[] identityHandles) {
        this.identityHandles = identityHandles;
    }

    public String getPersonSpace() {
        return personSpace;
    }

    public void setPersonSpace(String personSpace) {
        this.personSpace = personSpace;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public InternalHandle getInternalHandle() {
        return internalHandle;
    }

    public void setInternalHandle(InternalHandle internalHandle) {
        this.internalHandle = internalHandle;
    }

    @Override
    public String toString() {
        return "Person [identityHandles = " + identityHandles + ", " +
                "personSpace = " + personSpace + ", createdBy = " + createdBy + ", created = "
                + created + ", internalHandle = " + internalHandle + "]";
    }
}
