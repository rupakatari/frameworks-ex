package net.ruvetech.my.pojo;

public class IdentityHandles {
    private String visibility_marker;

    private String unique_identifier;

    private String interface_identifier;

    public String getVisibility_marker() {
        return visibility_marker;
    }

    public void setVisibility_marker(String visibility_marker) {
        this.visibility_marker = visibility_marker;
    }

    public String getUnique_identifier() {
        return unique_identifier;
    }

    public void setUnique_identifier(String unique_identifier) {
        this.unique_identifier = unique_identifier;
    }

    public String getInterface_identifier() {
        return interface_identifier;
    }

    public void setInterface_identifier(String interface_identifier) {
        this.interface_identifier = interface_identifier;
    }

    @Override
    public String toString() {
        return "IdentityHandles [visibility_marker = " + visibility_marker + ", unique_identifier = " + unique_identifier + ", interface_identifier = " + interface_identifier + "]";
    }
}