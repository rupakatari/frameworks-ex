package net.ruvetech.bdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.ruvetech.data.Store;
import net.thucydides.core.annotations.Shared;
import net.thucydides.core.annotations.Steps;

public class CommonStepDefinitions {

    @Shared
    private Store store;

    public CommonStepDefinitions() {
        //store = new Store();
    }

    @Given("Clive has ${int} in his Current account")
    public void cliveHas$InHisCurrentAccount(int amount) {

        //store.setTotalAmount(amount);
        System.out.println(store.toString());
    }

    @When("he withdraws ${int} in cash")
    public void heWithdraws$InCash(int amount) {
        //store.setWithDrawl(amount);
    }

}
