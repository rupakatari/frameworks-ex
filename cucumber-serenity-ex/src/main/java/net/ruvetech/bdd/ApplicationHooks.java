package net.ruvetech.bdd;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class ApplicationHooks {

    @Before
    public void beforeEveryScenario(Scenario scenario) {
        System.out.println("Scenario Name: " + scenario.getName());
        System.out.println("Feature Name: " + getFeatureName(scenario));
    }

    @After
    public void afterEveryScenario() {
        System.out.println("afterEveryScenario");
    }

    private String getFeatureName(Scenario scenario) {
        String featureName = "Feature ";
        String rawFeatureName = scenario.getId().split(";")[0].replace("-", " ");
        featureName = featureName + rawFeatureName.substring(0, 1).toUpperCase() + rawFeatureName.substring(1);
        return featureName;
    }
}
