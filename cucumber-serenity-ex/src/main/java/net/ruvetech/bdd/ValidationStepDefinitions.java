package net.ruvetech.bdd;

import io.cucumber.java.en.Then;
import net.ruvetech.data.Store;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class ValidationStepDefinitions {

    @Steps
    private Store store;

    public ValidationStepDefinitions() {
    }

    @Then("his remaining balance should be ${int}")
    public void hisRemainingBalanceShouldBe$(int remainingAmount) {
        Assert.assertEquals(store.getTotalAmount() - store.getWithDrawl(), remainingAmount);
        //System.out.println(store.toString());
    }
}
