package net.ruvetech.service;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.ruvetech.data.Store;

public class UserService {

    private Store store = null;

    public UserService(Store store) {
        this.store = store;
    }

    final String BASE_URL = "https://reqres.in/api/users/";

    public Response getUser(int userId) {
        RequestSpecification request = RestAssured.given();
        request.header("header1", "User1");
        store.setRequest(request);
        Response response = request.when().get(BASE_URL + userId);
        store.setResponse(response);
        return response;
    }

    public Response createUser(String json) {
        RequestSpecification request = RestAssured.given();
        store.setRequest(request);
        Response response = request.when().body(json).post(BASE_URL);
        store.setResponse(response);
        return response;
    }
}
