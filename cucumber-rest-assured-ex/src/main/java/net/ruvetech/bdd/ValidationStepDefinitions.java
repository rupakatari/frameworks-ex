package net.ruvetech.bdd;

import java.util.Map;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.ruvetech.data.Store;
import net.ruvetech.misc.Util;
import org.junit.Assert;

public class ValidationStepDefinitions {

    private Store store = null;

    public ValidationStepDefinitions(Store store) {
        this.store = store;
    }

    @Then("I should get response code as {int}")
    public void iShouldGetResponseCodeAs(int responseCode) {
        Assert.assertEquals(responseCode, store.getResponse().getStatusCode());
    }

    @And("created User has {string} attribute")
    public void createdUserHasAttribute(String jsonAttribute) {
        Map<String, String> map = Util.getResponse(store.getResponse());
        Assert.assertTrue(map.containsKey(jsonAttribute));
    }
}
