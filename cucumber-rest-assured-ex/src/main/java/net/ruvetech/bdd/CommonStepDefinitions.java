package net.ruvetech.bdd;

import java.util.List;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.ruvetech.data.Store;
import net.ruvetech.misc.Util;
import net.ruvetech.service.UserService;

public class CommonStepDefinitions {

    private Store store = null;

    public CommonStepDefinitions(Store store) {
        this.store = store;
    }

    @Given("Given I want to execute service {string}")
    public void givenIWantToExecuteService(String arg0) {
        //do nothing
    }

    @When("I request for user id {int}")
    public void iRequestForUserId(int userId) {
        UserService userService = new UserService(store);
        userService.getUser(userId);
    }


    @When("I create a new user with following info")
    public void iCreateANewUserWithFollowingInfo(DataTable dataTable) {
        List<List<String>> listList = dataTable.asLists();
        String name = listList.get(0).get(0);
        String job = listList.get(0).get(1);

        String json = Util.create(name, job);
        UserService userService = new UserService(store);
        userService.createUser(json);
    }
}
