package net.ruvetech.data;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Data;

@Data
public class Store {

    private RequestSpecification request;
    private Response response;

}
