package net.ruvetech.misc;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import net.ruvetech.data.User;

public class Util {

    public static String create(String name, String job) {

        User user = new User();
        user.setName(name);
        user.setJob(job);
        try {
            return new ObjectMapper().writeValueAsString(user);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static Map<String, String> getResponse(Response response) {
        String json = response.body().asString();
        System.out.println(json);
        try {
            return new ObjectMapper().readValue(json, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
