Feature: CRUD REST operations

  Scenario: Test a GET request
    Given Given I want to execute service "User Service"
    When I request for user id 2
    Then I should get response code as 200

  Scenario: Test a POST request
    Given Given I want to execute service "User Service"
    When I create a new user with following info
      | Rupa | QA |
    Then I should get response code as 201
    And created User has "id" attribute