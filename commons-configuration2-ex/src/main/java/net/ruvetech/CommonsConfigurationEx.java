package net.ruvetech;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.SystemConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 * Created by vemulav on 23/10/2017.
 */
public class CommonsConfigurationEx {

	public static void main(String[] args) {
		try {
			configuration();
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}

	public static void configuration() throws ConfigurationException {
		Parameters params = new Parameters();
		FileBasedConfigurationBuilder<PropertiesConfiguration> builder = new FileBasedConfigurationBuilder<PropertiesConfiguration>(
			PropertiesConfiguration.class)
				.configure(params.fileBased().setListDelimiterHandler(new DefaultListDelimiterHandler(','))
					.setFile(new File("app.properties")));

		CompositeConfiguration config = new CompositeConfiguration();
		config.addConfiguration(new SystemConfiguration());
		config.addConfiguration(builder.getConfiguration());

		System.out.println("----------------------------");
		System.out.println("Listing composite properties");
		System.out.println("----------------------------");
		Iterator<String> keys = config.getKeys();
		while (keys.hasNext()) {
			String key = keys.next();
			System.out.println(key + " = " + config.getProperty(key));
		}

		//read int property
		Integer integer = config.getInt("mail.smtp.port");

		//refer property file inside a property file
		String backgroundColour = (String)config.getProperty("colors.background");
		String[] colors = config.getStringArray("colors.pie");
		List<Object> colorList = config.getList("colors.pie");
	}
}
