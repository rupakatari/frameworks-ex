package net.ruvetech;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Created by vemulav on 24/10/2017.
 */
public class PropertiesUtil {

	public static void main(String[] args) throws IOException {
		InputStream is = PropertiesUtil.class.getResourceAsStream("/app.properties");
		Properties defaultProps = new Properties();
		defaultProps.load(is);
		is.close();

		// create application properties with default
		Properties applicationProps = new Properties(defaultProps);

		// now load properties
		// from last invocation
		is = PropertiesUtil.class.getResourceAsStream("/colors.properties");
		applicationProps.load(is);
		is.close();

		Properties systemProperties = System.getProperties();

		Enumeration keys = systemProperties.keys();
		while (keys.hasMoreElements()) {
			String key = (String)keys.nextElement();
			String value = (String)systemProperties.get(key);
			System.out.println(key + ": " + value);
		}

		System.out.println(applicationProps.getProperty("mail.smtp.port"));

	}
}
