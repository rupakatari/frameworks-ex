package net.ruvetech.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {

    protected WebDriver webDriver;
    private static final int WAIT_TIME = 10;
    private WebDriverWait webDriverWait;

    public BasePage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    protected void waitForElementToBeClickable(WebElement webElement) {
        webDriverWait = new WebDriverWait(webDriver, WAIT_TIME);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    protected void waitForVisibilityOf(WebElement webElement) {
        webDriverWait = new WebDriverWait(webDriver, WAIT_TIME);
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
    }
}