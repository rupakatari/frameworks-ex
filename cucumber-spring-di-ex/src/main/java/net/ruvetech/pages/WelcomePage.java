package net.ruvetech.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WelcomePage extends BasePage {

    @FindBy(how = How.XPATH, using = ".//*[@id='home']/div[3]/div/div/div[2]/div[3]/a")
    private WebElement loginElement;

    @FindBy(how = How.ID, using = "email")
    private WebElement emailInputField;

    @FindBy(how = How.ID, using = "password")
    private WebElement passwordInputField;

    @FindBy(how = How.XPATH, using = ".//*[@id='login_form']/div[4]/button")
    private WebElement logInButton;

    @FindBy(how = How.CLASS_NAME,using="hide-tip-onclick topnavlink nopadding")
    private WebElement logoutFieldElement;

    public WelcomePage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void selectLogin(boolean maximizeWindow) {
        waitForElementToBeClickable(loginElement);
        loginElement.click();

        if (maximizeWindow) {
            webDriver.manage().window().maximize();
        }

    }
    public void login(String email, String password)
    {
        waitForVisibilityOf(emailInputField);
        waitForVisibilityOf(passwordInputField);
        emailInputField.sendKeys(email);
        //((JavascriptExecutor)driver).executeScript("document.getElementById('password').value='"+password+"';");
        passwordInputField.sendKeys(password);
        waitForElementToBeClickable(logInButton);
        logInButton.click();
    }

    public void logOutTeamerApplication(){
        Select drpLogout = new Select(logoutFieldElement);
        drpLogout.selectByVisibleText("Logout");

        Actions actions = new Actions(webDriver);
        //	actions.moveToElement(logoutFieldElement).perform();

    }
}