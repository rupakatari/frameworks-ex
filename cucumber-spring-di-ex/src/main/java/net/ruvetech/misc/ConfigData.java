package net.ruvetech.misc;


import org.springframework.beans.factory.annotation.Value;
public class ConfigData {

    @Value("${url}")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
