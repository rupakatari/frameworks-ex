package net.ruvetech.bdd;

import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = "cucumber-glue")
public class ScenarioContext {

    private WebDriver webDriver;

    private String ScenarioName;
    private String featureName;

    public ScenarioContext() {

    }

    public String getScenarioName() {
        return ScenarioName;
    }

    protected void setScenarioName(String scenarioName) {
        ScenarioName = scenarioName;
    }

    public String getFeatureName() {
        return featureName;
    }

    protected void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    protected void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
}
