package net.ruvetech.bdd;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;

public class CucumberApplicationHooks {

    static {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/spring-di-ex/chromedriver");
    }

    @Autowired
    ScenarioContext scenarioContext;

    @Before
    public void beforeEveryScenario(Scenario scenario) {

        System.out.println("Checking Scenario Name: " + scenarioContext.getScenarioName());
        scenarioContext.setFeatureName(getFeatureName(scenario));
        scenarioContext.setScenarioName(scenario.getName());
        scenarioContext.setWebDriver(new ChromeDriver());
        System.out.println("Feature Name: " + scenarioContext.getFeatureName());
        System.out.println("Scenario Name1: " + scenarioContext.getScenarioName());
    }

    @After
    public void afterEveryScenario() {
        System.out.println("<<<<<<<<<<<<<<<<<<");
        System.out.println("afterEveryScenario");
        //scenarioContext.webDriver.quit();
    }

    private String getFeatureName(Scenario scenario) {
        String featureName = "Feature ";
        String rawFeatureName = scenario.getId().split(";")[0].replace("-", " ");
        featureName = featureName + rawFeatureName.substring(0, 1).toUpperCase() + rawFeatureName.substring(1);
        return featureName;
    }
}
