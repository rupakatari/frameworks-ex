package net.ruvetech.bdd;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.ruvetech.misc.ConfigData;
import net.ruvetech.pages.WelcomePage;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class CommonStepDefinitions {


    private WebDriver webDriver;
    private WelcomePage welcomePage;
    private boolean statePreserved = false;

    @Autowired
    private ConfigData configData;


    public CommonStepDefinitions(ScenarioContext scenarioContext) {
        webDriver = scenarioContext.getWebDriver();
        welcomePage = new WelcomePage(webDriver);
    }

    @Given("^User is on Home Page$")
    public void user_is_on_Home_Page() throws Throwable {
        webDriver.navigate().to(configData.getUrl());
        statePreserved = true;
    }

    @When("^User Navigate to LogIn Page$")
    public void user_Navigate_to_LogIn_Page() throws Throwable {
        welcomePage.selectLogin(true);
    }

    @When("^User enters Credentials to LogIn$")
    public void user_enters_Credentials_to_LogIn(DataTable dataTable) throws Throwable {
        List<List<String>> userCredentials = dataTable.raw();
        List<String> firstRow = userCredentials.get(0);
        String userName = firstRow.get(0);
        String password = firstRow.get(1);
        login(userName, password);
    }

    public void login(String userName, String password) {
        welcomePage.login(userName, password);
    }

    @Then("^Message displayed Login Successfully$")
    public void message_displayed_Login_Successfully() throws Throwable {
        System.out.println("User logs in successfully");
        System.out.println("statePreserved " + statePreserved);
    }
}
