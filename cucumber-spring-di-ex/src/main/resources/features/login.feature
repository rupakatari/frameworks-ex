Feature: Logs into the Teamer application

  @smoke
  Scenario: Login with inValid Credentials
    Given User is on Home Page
    When User Navigate to LogIn Page
    And User enters Credentials to LogIn
      | xxxxx@gmail.com | xxxxxxxx |
    Then Message displayed Login Successfully

  @smoke
  Scenario: Login with dummy credentials
    Given User is on Home Page
    When User Navigate to LogIn Page
    And User enters Credentials to LogIn
      | xxxxx@gmail.com | xxxxxxxx |
    Then Message displayed Login Successfully