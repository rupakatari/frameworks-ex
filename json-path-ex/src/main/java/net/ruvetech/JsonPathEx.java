package net.ruvetech;

import com.jayway.jsonpath.JsonPath;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonPathEx {

    public static void main(String[] args) throws IOException {
        String path = System.getProperty("user.dir") + "/json-path-ex/src/main/resources/";
        File file = new File(path + "sample.json");
        String jsonStr = FileUtils.readFileToString(file);
        String results[] = getEventIDs(jsonStr);
        for (String eventId : results) {
            System.out.println(eventId);
        }
    }

    public static String[] getEventIDs(String json) {
        List<String> results = JsonPath.read(json, "$.[*].event_type");
        return results.toArray(new String[0]);
    }
}
